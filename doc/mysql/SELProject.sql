-- MySQL Workbench Forward Engineering
-- Autor : Ledauphin Saidain Estabel

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema project_LSE
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema project_LSE
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `project_LSE` DEFAULT CHARACTER SET utf8 ;
USE `project_LSE` ;

-- -----------------------------------------------------
-- Table `project_LSE`.`campus`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `project_LSE`.`campus` (
  `id` INT UNIQUE NOT NULL AUTO_INCREMENT,
  `label` VARCHAR(45) NOT NULL,
  `departement` INT NOT NULL,
  PRIMARY KEY (`id`));


-- -----------------------------------------------------
-- Table `project_LSE`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `project_LSE`.`user` (
  `id` INT UNIQUE NOT NULL AUTO_INCREMENT,
  `fistname` VARCHAR(45) NOT NULL,
  `lastname` VARCHAR(45) NOT NULL,
  `birthdate` DATE NOT NULL,
  `campus_id` INT UNIQUE NOT NULL,
  PRIMARY KEY (`id`, `campus_id`),
  INDEX `fk_user_campus1_idx` (`campus_id` ASC),
  CONSTRAINT `fk_user_campus1`
    FOREIGN KEY (`campus_id`)
    REFERENCES `project_LSE`.`campus` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `project_LSE`.`tutor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `project_LSE`.`tutor` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT UNIQUE NOT NULL,
  PRIMARY KEY (`id`, `user_id`),
  INDEX `fk_tutor_person1_idx` (`user_id` ASC),
  CONSTRAINT `fk_tutor_person1`
    FOREIGN KEY (`user_id`)
    REFERENCES `project_LSE`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `project_LSE`.`promotion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `project_LSE`.`promotion` (
  `id` INT UNIQUE NOT NULL AUTO_INCREMENT,
  `label` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));


-- -----------------------------------------------------
-- Table `project_LSE`.`student`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `project_LSE`.`student` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT UNIQUE NOT NULL,
  `promotion_id` INT UNIQUE NOT NULL,
  PRIMARY KEY (`id`, `user_id`, `promotion_id`),
  INDEX `fk_student_person1_idx` (`user_id` ASC),
  INDEX `fk_student_promotion1_idx` (`promotion_id` ASC),
  CONSTRAINT `fk_student_person1`
    FOREIGN KEY (`user_id`)
    REFERENCES `project_LSE`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_promotion1`
    FOREIGN KEY (`promotion_id`)
    REFERENCES `project_LSE`.`promotion` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `project_LSE`.`admin`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `project_LSE`.`admin` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT UNIQUE NOT NULL,
  PRIMARY KEY (`id`, `user_id`),
  INDEX `fk_admin_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_admin_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `project_LSE`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `project_LSE`.`group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `project_LSE`.`group` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `label` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));


-- -----------------------------------------------------
-- Table `project_LSE`.`diploma`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `project_LSE`.`diploma` (
  `id` INT UNIQUE NOT NULL AUTO_INCREMENT,
  `level` INT NOT NULL,
  `label` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));


-- -----------------------------------------------------
-- Table `project_LSE`.`student_has_diploma`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `project_LSE`.`student_has_diploma` (
  `diploma_id` INT UNIQUE NOT NULL,
  `student_id` INT NOT NULL,
  PRIMARY KEY (`diploma_id`, `student_id`),
  INDEX `fk_diplome_has_etudiant_etudiant1_idx` (`student_id` ASC),
  INDEX `fk_diplome_has_etudiant_diplome_idx` (`diploma_id` ASC),
  CONSTRAINT `fk_diplome_has_etudiant_diplome`
    FOREIGN KEY (`diploma_id`)
    REFERENCES `project_LSE`.`diploma` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_diplome_has_etudiant_etudiant1`
    FOREIGN KEY (`student_id`)
    REFERENCES `project_LSE`.`student` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `project_LSE`.`student_has_group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `project_LSE`.`student_has_group` (
  `group_id` INT NOT NULL,
  `student_id` INT NOT NULL,
  PRIMARY KEY (`group_id`, `student_id`),
  INDEX `fk_groupe_has_etudiant_etudiant1_idx` (`student_id` ASC),
  INDEX `fk_groupe_has_etudiant_groupe1_idx` (`group_id` ASC),
  CONSTRAINT `fk_groupe_has_etudiant_groupe1`
    FOREIGN KEY (`group_id`)
    REFERENCES `project_LSE`.`group` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_groupe_has_etudiant_etudiant1`
    FOREIGN KEY (`student_id`)
    REFERENCES `project_LSE`.`student` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `project_LSE`.`dropbox`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `project_LSE`.`dropbox` (
  `id` INT UNIQUE NOT NULL AUTO_INCREMENT,
  `date_Create` DATE NOT NULL,
  `date_End` DATE NOT NULL,
  `tutor_id` INT NOT NULL,
  `tutor_user_id` INT UNIQUE NOT NULL,
  `campus_id` INT UNIQUE NOT NULL,
  PRIMARY KEY (`id`, `tutor_id`, `tutor_user_id`, `campus_id`),
  INDEX `fk_dropbox_tutor1_idx` (`tutor_id` ASC, `tutor_user_id` ASC),
  INDEX `fk_dropbox_campus1_idx` (`campus_id` ASC),
  CONSTRAINT `fk_dropbox_tutor1`
    FOREIGN KEY (`tutor_id` , `tutor_user_id`)
    REFERENCES `project_LSE`.`tutor` (`id` , `user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_dropbox_campus1`
    FOREIGN KEY (`campus_id`)
    REFERENCES `project_LSE`.`campus` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `project_LSE`.`document`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `project_LSE`.`document` (
  `id` INT UNIQUE NOT NULL AUTO_INCREMENT,
  `label` VARCHAR(45) NOT NULL,
  `date_Create` DATE NOT NULL,
  `date_Update` DATE NOT NULL,
  `student_id` INT NOT NULL,
  `student_user_id` INT UNIQUE NOT NULL,
  `student_promotion_id` INT UNIQUE NOT NULL,
  `group_id` INT NOT NULL,
  `dropbox_id` INT UNIQUE NOT NULL,
  PRIMARY KEY (`id`, `student_id`, `student_user_id`, `student_promotion_id`, `group_id`, `dropbox_id`),
  INDEX `fk_document_student1_idx` (`student_id` ASC, `student_user_id` ASC, `student_promotion_id` ASC),
  INDEX `fk_document_group1_idx` (`group_id` ASC),
  INDEX `fk_document_dropbox1_idx` (`dropbox_id` ASC),
  CONSTRAINT `fk_document_student1`
    FOREIGN KEY (`student_id` , `student_user_id` , `student_promotion_id`)
    REFERENCES `project_LSE`.`student` (`id` , `user_id` , `promotion_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_document_group1`
    FOREIGN KEY (`group_id`)
    REFERENCES `project_LSE`.`group` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_document_dropbox1`
    FOREIGN KEY (`dropbox_id`)
    REFERENCES `project_LSE`.`dropbox` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
