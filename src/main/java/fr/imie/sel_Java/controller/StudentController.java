package fr.imie.sel_Java.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import fr.imie.sel_Java.dao.DocumentDAO;
import fr.imie.sel_Java.dao.HomeworkDAO;
import fr.imie.sel_Java.dao.PromotionDAO;
import fr.imie.sel_Java.dao.TopicDAO;
import fr.imie.sel_Java.dao.UserDAO;
import fr.imie.sel_Java.entity.Document;
import fr.imie.sel_Java.entity.Homework;

@Controller
public class StudentController {

	@Autowired @Qualifier("DocumentDAO")
    protected DocumentDAO documentDAO;
	@Autowired @Qualifier("UserDAO")
    protected UserDAO userDAO;
	@Autowired @Qualifier("TopicDAO")
    protected TopicDAO topicDAO;
	@Autowired @Qualifier("PromotionDAO")
    protected PromotionDAO promotionDAO;
	@Autowired @Qualifier("HomeworkDAO")
    protected HomeworkDAO homeworkDAO;
	
	
    private String message;
    private Integer selectTopic;
    private Integer selectHomework;
	
    public void refreshValues(Model model){
    	try{
		model.addAttribute("message",message);
		model.addAttribute("user", LoginController.userAuth);
		model.addAttribute("allTopics", LoginController.userAuth.getPromotion().getTopics());
		model.addAttribute("allPromotions", promotionDAO.getAll());
		
		if(selectHomework != null){
			model.addAttribute("selectedHomework", homeworkDAO.getById(selectHomework));
			if(homeworkDAO.getById(selectHomework).getDocuments()!=null){
				model.addAttribute("allDocs", homeworkDAO.getById(selectHomework).getDocuments());
			}
		}
		model.addAttribute("selectedTopic",selectTopic);
    	} catch (Exception e){
    		e.printStackTrace();
    	}
    }
    
	@RequestMapping(method=RequestMethod.GET, path="/student")
	public String student(Model model){
		refreshValues(model);
		
		return "student";
	}
	
	@RequestMapping(method = RequestMethod.POST, path="/studentSelectTopic")
	public String selectTopic(Model model, HttpServletRequest request){
		selectTopic = Integer.parseInt(request.getParameter("selectedTopic"));
		message="";
		
		refreshValues(model);
		
		return "student";
	}

	   @RequestMapping(method = RequestMethod.POST, path="/studentCreateHomework")
	    public String createHomework(Model model, HttpServletRequest request,
	    		@RequestParam(value="selectedTopic", required=false)String selectedTopic){
		   
	    	String nameHomework = request.getParameter("nameHomework");
	    	
	    	if(selectedTopic==null){
	    		if(selectTopic==null){
					message="missingUploadDocuments";
					refreshValues(model);
					
					return "student";
	    		} else{
	    			selectedTopic = Integer.toString(selectTopic);
	    		}
	    	}
	    	
		    try {
		    	Homework homework = new Homework();
		    	homework.setTitle(nameHomework);
		    	homework.setTopic(topicDAO.getById(Integer.parseInt(selectedTopic)));
		    	topicDAO.getById(Integer.parseInt(selectedTopic)).getHomeworks().add(homework);
		    	topicDAO.update(topicDAO.getById(Integer.parseInt(selectedTopic)));
		    	
		    	LoginController.userAuth.getHomeworks().add(homework);
		    	LoginController.userAuth = userDAO.update(LoginController.userAuth);
		    	System.out.println("HERE------------------------------------------");
		    	System.out.println(LoginController.userAuth.findHomeworkByTitle(nameHomework).getTopic());
		    	
		    	selectTopic = Integer.parseInt(selectedTopic);
		    	selectHomework = LoginController.userAuth.findHomeworkByTitle(nameHomework).getId();
		    			
		    	message="successCreateHomework";
		      } catch (Exception e) {
		        e.printStackTrace();
		        message="failedCreateHomework";
		      }

		    refreshValues(model);
	    	
	    	return "student";
	    }
	   
	 @RequestMapping(method = RequestMethod.POST, path="/studentUpload")
	    public String uploadDocuments(@RequestParam("uploadFiles[]") ArrayList<MultipartFile> filesToUpload,
	    		@RequestParam(value="selectedTopic", required=false)String selectedTopic, Model model) {
	    	
	    	String home = System.getProperty("user.home");
	    	
	    	if(selectedTopic==null){
	    		if(selectTopic==null || selectHomework==null){
					message="missingUploadDocuments";
					refreshValues(model);
					
					return "tutor";
	    		} else{
	    			selectedTopic = Integer.toString(selectTopic);
	    		}
	    	}
	    	
	    	try {
	    		
	    		ArrayList<Document> documentsToUpload = new ArrayList<Document>();
	    		
	    		for(MultipartFile fileToUpload:filesToUpload){
	    			
	    			new File(home + "/Downloads/Server/"+ LoginController.userAuth.getsAMAccountName()
	    			+ "/" + topicDAO.getById(Integer.parseInt(selectedTopic)).getName() + "/" + homeworkDAO.getById(selectHomework).getTitle()).mkdirs();
	        		
	    			File file = new File(home + "/Downloads/Server/"
	        		+ LoginController.userAuth.getsAMAccountName() + "/" + topicDAO.getById(Integer.parseInt(selectedTopic)).getName()
	        		+ "/" + homeworkDAO.getById(selectHomework).getTitle() + "/" + fileToUpload.getOriginalFilename());
	        		
	        		boolean checkDuplicateByPath = homeworkDAO.getById(selectHomework).getDocuments().stream().anyMatch(doc -> file.getAbsolutePath().equals(doc.getPath()));  
	        		if(!checkDuplicateByPath){
	        			
	            		file.createNewFile();
	            		fileToUpload.transferTo(file);

	            		Document document = new Document();
	            		document.setTitle(fileToUpload.getOriginalFilename());
	            		document.setPath(file.getAbsolutePath());
	            		document.setAuthor(LoginController.userAuth);
	            		
	            		documentsToUpload.add(document);           		         	
	        		}

	    		}
	    		
	    		if(documentsToUpload.size()>0){
	    			LoginController.userAuth.findHomeworkById(selectHomework).getDocuments().addAll(documentsToUpload);
	    			LoginController.userAuth.getDocuments().addAll(documentsToUpload);
	    			LoginController.userAuth = userDAO.update(LoginController.userAuth);
	    			
	        		message = "successUpload";
	    		}
	    		
	    	}catch(ConstraintViolationException e){
				message = "duplicateUpload";			
			} catch (IOException e) {
				e.printStackTrace();
				message = "failedUpload";
			} catch (Exception e){
				e.printStackTrace();
				message = "failedUpload";
			} finally{
				model.addAttribute("uploadFiles[]", "");
				filesToUpload = null;
			}
	    	
	    	selectTopic = Integer.parseInt(selectedTopic);
	    	
	    	refreshValues(model);
	    	
	        return "student";
	    }
	 
	   @RequestMapping(method = RequestMethod.POST, path="/studentDownloadDocuments")
	    public String downloadDocuments(Model model, HttpServletRequest request){
	    	String home = System.getProperty("user.home");
	    	
	    	if(request.getParameterValues("selectedFile")==null){
	    		message="unselectDownload";
	    		
	        	refreshValues(model);
	        	
	        	return "student";
	    	}
	    	
	    	ArrayList<String> filesToDownload = new ArrayList<String>(Arrays.asList(request.getParameterValues("selectedFile")));
	    	
	    	for (String fileToDownload : filesToDownload) {
	    	    try {
	    	    	File originFile = new File(fileToDownload);
	    	    	int indexStart = fileToDownload.lastIndexOf("\\");
	    	    	File newFile = new File(home + "/Downloads/myDl/" + LoginController.userAuth.getsAMAccountName() + "/" + fileToDownload.substring(indexStart));
	    	    	FileUtils.copyFile(originFile, newFile);
	    	    	newFile.createNewFile();
	    	    	
	    	    	message="successDownload";
	    	      } catch (IOException e) {
	    	        e.printStackTrace();
	    	        message="failedDownload";
	    	      }
			}
	    	
	    	refreshValues(model);
	    	
	    	return "student";
	    }
	   
	   @RequestMapping(method = RequestMethod.POST, path="/studentDeleteDocuments")
	    public String deleteDocuments(Model model, HttpServletRequest request){
		   ArrayList<String> filesToDelete = new ArrayList<String>(Arrays.asList(request.getParameterValues("selectedFile")));

	    	try{
	    		for(String fileToDelete:filesToDelete){

    				LoginController.userAuth.removeDocumentByPath(fileToDelete);
    				
					for (Document doc : homeworkDAO.getById(selectHomework).getDocuments()) {
						if(doc.getPath().equals(fileToDelete)){
							homeworkDAO.getById(selectHomework).removeDocumentByPath(fileToDelete);
							break;
						}
					}
    				
    				homeworkDAO.update(homeworkDAO.getById(selectHomework));
    				LoginController.userAuth = userDAO.update(LoginController.userAuth);
    				documentDAO.delete(documentDAO.findByPath(fileToDelete));
    				File file = new File(fileToDelete);
    				file.delete();
    				
    				message = "successDelete";
	    		}
	        	
	    	} catch(Exception e){
	    		e.printStackTrace();
	        	message = "failedDelete";
	    	} finally{
	    		filesToDelete = null;
	    	}

	    	refreshValues(model);
	    	
	    	return "student";
	    }
	   
}
