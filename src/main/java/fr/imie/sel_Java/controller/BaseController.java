package fr.imie.sel_Java.controller;

import fr.imie.sel_Java.dao.BaseDAO;
import fr.imie.sel_Java.entity.EntityBase;

public class BaseController<T extends EntityBase> extends BaseDAO<T>{
    
	public T selectItem(T item){	
		return getById(item.getId());
	}
	
}
