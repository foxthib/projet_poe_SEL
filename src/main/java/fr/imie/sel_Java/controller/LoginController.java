package fr.imie.sel_Java.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import fr.imie.sel_Java.dao.UserDAO;
import fr.imie.sel_Java.entity.Student;
import fr.imie.sel_Java.entity.Tutor;
import fr.imie.sel_Java.entity.User;

@Controller
@EnableWebSecurity
public class LoginController{
	
	@Autowired @Qualifier("UserDAO")
    protected UserDAO userDao;
    public static User userAuth;
    
    @RequestMapping(method=RequestMethod.GET, path="/login")
	public String login(Model model, HttpServletRequest request){
    	model.addAttribute("fail", "false");
    	
    	 String url = request.getQueryString(); 
    	 if(url != null && url.contains("logout=true")){
    		 System.out.println("userAuth=null");
    		 userAuth = null;
    	 }

		return "login";
	}
    
    @RequestMapping(method=RequestMethod.GET, path="/logout")
    public String logout(
            Model model) {
        return "login";
    }
	
	@RequestMapping(method=RequestMethod.POST, path="/login")
	public String setLogin(@RequestParam(name="username") String username,@RequestParam(name="password") String password, Model model){
		
		User user = this.userDao.findBysAMAccountName(username);
		
		if(user != null && user.getsAMAccountName().equals(username) && user.getPassword().equals(password)){

			userAuth = this.userDao.findBysAMAccountName(username);
			
			if(user instanceof Tutor) return "redirect:tutor";
			else if (user instanceof Student)  return "redirect:student";
		}
		else{
			model.addAttribute("fail", "true");
		}
		
		return "login";
	}
}
