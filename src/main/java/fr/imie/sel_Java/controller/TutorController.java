package fr.imie.sel_Java.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import fr.imie.sel_Java.dao.DocumentDAO;
import fr.imie.sel_Java.dao.PromotionDAO;
import fr.imie.sel_Java.dao.TopicDAO;
import fr.imie.sel_Java.dao.UserDAO;
import fr.imie.sel_Java.entity.Document;
import fr.imie.sel_Java.entity.Topic;

@Controller
public class TutorController extends BaseController{

	@Autowired @Qualifier("DocumentDAO")
    protected DocumentDAO documentDAO;
	@Autowired @Qualifier("UserDAO")
    protected UserDAO userDAO;
	@Autowired @Qualifier("TopicDAO")
    protected TopicDAO topicDAO;
	@Autowired @Qualifier("PromotionDAO")
    protected PromotionDAO promotionDAO;
	
    private String message;
    private Integer selectTopic;
	
    public void refreshValues(Model model){
		model.addAttribute("message",message);
		model.addAttribute("user", LoginController.userAuth);
		model.addAttribute("allTopics", LoginController.userAuth.getTopics());
		model.addAttribute("allPromotions", promotionDAO.getAll());
		
		if(selectTopic != null){
			model.addAttribute("allDocs",LoginController.userAuth.findTopicById(selectTopic).getDocuments());
			model.addAttribute("allHomeworks", topicDAO.getById(selectTopic).getHomeworks());
		}
		model.addAttribute("selectedTopic",selectTopic);
    }
    
	@RequestMapping(method=RequestMethod.GET, path="/tutor")
	public String tutor(Model model){
		refreshValues(model);
			
		return "tutor";
	}
	
    @RequestMapping(method = RequestMethod.POST, path="/tutorUpload")
    public String uploadDocuments(@RequestParam("uploadFiles[]") ArrayList<MultipartFile> filesToUpload,
    		@RequestParam(value="selectedTopic", required=false)String selectedTopic, Model model) {
    	
    	String home = System.getProperty("user.home");
    	
    	if(selectedTopic==null){
    		if(selectTopic==null){
				message="missingUploadDocuments";
				refreshValues(model);
				
				return "tutor";
    		} else{
    			selectedTopic = Integer.toString(selectTopic);
    		}
    	}
    	
    	try {
    		
    		ArrayList<Document> documentsToUpload = new ArrayList<Document>();
    		
    		for(MultipartFile fileToUpload:filesToUpload){
    			
    			new File(home + "/Downloads/Server/"+ LoginController.userAuth.getsAMAccountName()
    			+ "/" + topicDAO.getById(Integer.parseInt(selectedTopic)).getName()).mkdirs();
        		File file = new File(home + "/Downloads/Server/"
        		+ LoginController.userAuth.getsAMAccountName() + "/" + topicDAO.getById(Integer.parseInt(selectedTopic)).getName() + "/" + fileToUpload.getOriginalFilename());
        		
        		boolean checkDuplicateByPath = topicDAO.getById(Integer.parseInt(selectedTopic)).getDocuments().stream().anyMatch(doc -> file.getAbsolutePath().equals(doc.getPath()));  
        		if(!checkDuplicateByPath){
        			
            		file.createNewFile();
            		fileToUpload.transferTo(file);

            		Document document = new Document();
            		document.setTitle(fileToUpload.getOriginalFilename());
            		document.setPath(file.getAbsolutePath());
            		document.setAuthor(LoginController.userAuth);
            		
            		documentsToUpload.add(document);           		         	
        		}

    		}
    		
    		if(documentsToUpload.size()>0){
    			LoginController.userAuth.findTopicById(Integer.parseInt(selectedTopic)).getDocuments().addAll(documentsToUpload);
    			LoginController.userAuth.getDocuments().addAll(documentsToUpload);
    			LoginController.userAuth = userDAO.update(LoginController.userAuth);
    			
        		message = "successUpload";
    		}
    		
    	}catch(ConstraintViolationException e){
			message = "duplicateUpload";			
		} catch (IOException e) {
			e.printStackTrace();
			message = "failedUpload";
		} catch (Exception e){
			e.printStackTrace();
			message = "failedUpload";
		} finally{
			model.addAttribute("uploadFiles[]", "");
			filesToUpload = null;
		}
    	
    	selectTopic = Integer.parseInt(selectedTopic);
    	
    	refreshValues(model);
    	
        return "tutor";
    } 

	@RequestMapping(method = RequestMethod.POST, path="/tutorSelectTopic")
	public String selectItem(Model model, HttpServletRequest request){
		String selectItem = "selectTopic";
		selectTopic = super.selectItem(topicDAO.getById(Integer.parseInt(request.getParameter(selectItem)))).getId();
		
		return "tutor";
	}
	
    @RequestMapping(method = RequestMethod.POST, path="/tutorDownloadDocuments")
    public String downloadDocuments(Model model, HttpServletRequest request){
    	String home = System.getProperty("user.home");
    	
    	if(request.getParameterValues("selectedFile")==null){
    		message="unselectDownload";
    		
        	refreshValues(model);
        	
        	return "tutor";
    	}
    	
    	ArrayList<String> filesToDownload = new ArrayList<String>(Arrays.asList(request.getParameterValues("selectedFile")));
    	
    	for (String fileToDownload : filesToDownload) {
    	    try {
    	    	File originFile = new File(fileToDownload);
    	    	int indexStart = fileToDownload.lastIndexOf("\\");
    	    	File newFile = new File(home + "/Downloads/myDl/" + LoginController.userAuth.getsAMAccountName() + "/" + fileToDownload.substring(indexStart));
    	    	FileUtils.copyFile(originFile, newFile);
    	    	newFile.createNewFile();
    	    	
    	    	message="successDownload";
    	      } catch (IOException e) {
    	        e.printStackTrace();
    	        message="failedDownload";
    	      }
		}
    	
    	refreshValues(model);
    	
    	return "tutor";
    }
    
    
    @RequestMapping(method = RequestMethod.POST, path="/tutorDeleteDocuments")
    public String deleteDocuments(Model model, HttpServletRequest request){
    	ArrayList<String> filesToDelete = new ArrayList<String>(Arrays.asList(request.getParameterValues("selectedFile")));

    	try{
    		for(String fileToDelete:filesToDelete){
    			
    			boolean checkContainDocByPath = LoginController.userAuth.getDocuments().stream().anyMatch(doc -> fileToDelete.equals(doc.getPath()));  
    			if(checkContainDocByPath){
    				LoginController.userAuth.removeDocumentByPath(fileToDelete);
    				
    				for(Topic topic:LoginController.userAuth.getTopics()){
    					for (Document doc : topic.getDocuments()) {
							if(doc.getPath().equals(fileToDelete)){
								topic.removeDocumentByPath(fileToDelete);
								break;
							}
						}
    				}
    				
    				LoginController.userAuth = userDAO.update(LoginController.userAuth);
    				documentDAO.delete(documentDAO.findByPath(fileToDelete));
    				File file = new File(fileToDelete);
    				file.delete();
    				
    				message = "successDelete";
    			}
    			else{
    				message = "failedDelete";
    			}
    		}
        	
    	} catch(Exception e){
    		e.printStackTrace();
        	message = "failedDelete";
    	} finally{
    		filesToDelete = null;
    	}

    	refreshValues(model);
    	
    	return "tutor";
    }
    

    @RequestMapping(method = RequestMethod.POST, path="/tutorCreateTopic")
    public String createTopic(Model model, HttpServletRequest request){
    	String deadlineTopic = request.getParameter("deadlineTopic");
    	String nameTopic = request.getParameter("nameTopic");
    	String promotionTopic = request.getParameter("promotionTopic");
    	
    	
    	if(deadlineTopic.equals("") || nameTopic.equals("") || promotionTopic.equals("")){
    		message="missingCreateTopic";
    		
    		refreshValues(model);
    		
    		return "tutor";
    	}
    	
	    try {
	    	Topic topic = new Topic();
	    	topic.setName(nameTopic);
	    	topic.setAuthor(LoginController.userAuth);
	    	Date dateDeadline = new Date(deadlineTopic);
	    	topic.setDeadline(dateDeadline);
	    	
	    	promotionDAO.getById(Integer.parseInt(promotionTopic)).getTopics().add(topic);
	    	promotionDAO.update(promotionDAO.getById(Integer.parseInt(promotionTopic)));
	    	LoginController.userAuth.getTopics().add(topic);
	    	
	    	LoginController.userAuth = userDAO.update(LoginController.userAuth);
		    selectTopic = topic.getId();
	    	
	    	message="successCreateTopic";
	      } catch (IllegalArgumentException e){
	    	  message="fromatFailedCreateTopic";
	      } catch (Exception e) {
	        e.printStackTrace();
	        message="failedCreateTopic";
	      }
	    
	    refreshValues(model);
    	
    	return "tutor";
    }
    

    @RequestMapping(method = RequestMethod.POST, path="/tutorDeleteTopic")
    public String deleteTopic(Model model, HttpServletRequest request){
    	String selectedTopic = request.getParameter("selectedTopic");
    	String home = System.getProperty("user.home");
    	Integer idTopicToDelete = Integer.parseInt(selectedTopic);
    	
	    try {
	    		
    		for(int i = 0; i < LoginController.userAuth.getTopics().size();i++){
    			if(LoginController.userAuth.getTopics().get(i).getId().equals(idTopicToDelete)){
    				LoginController.userAuth.getTopics().remove(i);
    				LoginController.userAuth = userDAO.update(LoginController.userAuth);
    				break;
    			}
    		}
    		FileUtils.deleteDirectory(new File(home + "/Downloads/Server/" + LoginController.userAuth.getsAMAccountName() + "/" + topicDAO.getById(Integer.parseInt(selectedTopic)).getName()));
	    	
	    	topicDAO.delete(topicDAO.getById(idTopicToDelete));  	
	    	
	    	message="successDeleteTopic";
	    } catch (Exception e) {
	        e.printStackTrace();
	        message="failedDeleteTopic";
	    }
	    
	    selectTopic = null;
	    
	    refreshValues(model);
    	
    	return "tutor";
    }
    
    @RequestMapping(method = RequestMethod.POST, path="/tutorDownloadHomework")
    public String downloadHomework(Model model, HttpServletRequest request){
    	String home = System.getProperty("user.home");
    	
    	if(request.getParameterValues("selectedHomework")==null){
    		message="unselectDownload";
    		
        	refreshValues(model);
        	
        	return "tutor";
    	}
    	
    	ArrayList<String> filesToDownload = new ArrayList<String>(Arrays.asList(request.getParameterValues("selectedHomework")));
    	
    	for (String fileToDownload : filesToDownload) {
    	    try {
    	    	File originFile = new File(fileToDownload);
    	    	int indexStart = fileToDownload.lastIndexOf("\\");
    	    	
    	    	File newFile = new File(home + "/Downloads/myDl/" + LoginController.userAuth.getsAMAccountName() + "/" + fileToDownload.substring(indexStart));
    	    	FileUtils.copyFile(originFile, newFile);
    	    	newFile.createNewFile();
    	    	
    	    	message="successDownload";
    	      } catch (IOException e) {
    	        e.printStackTrace();
    	        message="failedDownload";
    	      }
		}
    	
    	refreshValues(model);
    	
    	return "tutor";
    }
}
