package fr.imie.sel_Java.utils;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.imie.sel_Java.entity.User;
import fr.imie.sel_Java.entity.UserList;

public class JsonManager {

	private JsonManager(){
		
	}
	
	private static JsonManager IMPLEMENT = new JsonManager();
	private ObjectMapper mapper = new ObjectMapper();

	public static JsonManager getJsonManager(){
		return IMPLEMENT;
	}

	public void createJsonUser(Object user, File file){

		try {
			mapper.writeValue(file, user);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public UserList usersFromJsonFile(File file){
		UserList userFromJsonFile = new UserList();

		try {
			userFromJsonFile.getUsers().add(mapper.readValue(new File(file.getAbsolutePath()), User.class));
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return userFromJsonFile;
	}

}
