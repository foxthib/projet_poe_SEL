package fr.imie.sel_Java.utils;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.directory.Attributes;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

/**
 * Connect and explore the ldap start to LDAP_Filter
 */
public class Ldap {

	private List<Attributes> allResults;

	public List<Attributes> ConnectToLdap(){

		allResults = new ArrayList<Attributes>();

		try {
			String PASSWORD = "Pr0jetJuJube";
			String LOGIN = "ldap_read";
			String PROVIDER_URL = "ldap://192.168.3.250:389/";
			//specify the LDAP search filter
			String searchFilter = "(objectClass=Person)";	
			//Specify the Base for the search
			String searchBase = "OU=Utilisateurs,OU=Formation,OU=RENNES,OU=sites,DC=imie,DC=lan";	

			Hashtable<String, String> ldapEnv = new Hashtable<String, String>(11);
			ldapEnv.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
			ldapEnv.put("com.sun.jndi.ldap.connect.timeout", "5000");
			ldapEnv.put(Context.PROVIDER_URL, PROVIDER_URL);
			ldapEnv.put("java.naming.ldap.attributes.binary", "objectGUID");
			ldapEnv.put(Context.SECURITY_AUTHENTICATION, "Simple");
			ldapEnv.put(Context.SECURITY_PRINCIPAL, LOGIN);
			ldapEnv.put(Context.SECURITY_CREDENTIALS, PASSWORD);

			InitialDirContext ldapContext = new InitialDirContext(ldapEnv);

			// Create the search controls         
			SearchControls searchCtls = new SearchControls();

			//Specify the attributes to return
			String[] returnedAtts ={"sn","givenName","mail", "sAMAccountName", "objectGUID", "memberOf", "userPassword"};
			searchCtls.setReturningAttributes(returnedAtts);

			//Specify the search scope
			searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

			// Search for objects using the filter
			NamingEnumeration<SearchResult> answer = ldapContext.search(searchBase, searchFilter, searchCtls);

			//Loop through the search results
			while (answer.hasMoreElements())
			{
				Attributes attrs = answer.next().getAttributes();
				if(attrs != null) allResults.add(attrs);
			}

			//TranslateToFile(allResults);

			ldapContext.close();
		}
		catch (Exception e)
		{
			System.out.println(" Search error: " + e);
			e.printStackTrace();
		}
		
		return allResults;
	}

/*	
	import java.io.File;
	import java.io.FileWriter;
	private void TranslateToFile(List<Attributes> list) throws Exception{

		File file = new File("C:\\Users\\thib\\Desktop\\List.txt");
		file.createNewFile();
		FileWriter fileWriter = new FileWriter(file);
		
		for (Attributes attrs:list)
		{
			fileWriter.write(attrs.get("givenName").get() + "\n");

			if(attrs.get("sn") != null) fileWriter.write(attrs.get("sn").get() + "\n");

			if (attrs.get("samAccountName") != null)  fileWriter.write(attrs.get("samAccountName").get() + "\n");

			if (attrs.get("mail") != null)  fileWriter.write(attrs.get("mail").get() + "\n");

			if(attrs.get("objectGUID") != null){
				byte[] guid = (byte[]) attrs.get("objectGUID").get();
				String guidId = convertToBindingString(guid);
				fileWriter.write(guidId + "\n");	
			}
			
			if(attrs.get("memberOf") != null) fileWriter.write(attrs.get("memberOf").get() + "\n");

			fileWriter.write("\n");
		}
		fileWriter.close();

	}
*/


	/*private void recursiveLDAPExplorer(LdapContext ctx, NamingEnumeration<SearchResult> userAnswer) throws NamingException{
		
		while(userAnswer.hasMoreElements()){
			SearchResult userAnswerTemp = userAnswer.next();				
			
			if(userAnswerTemp.getName().startsWith("OU")){
				if(!allPaths.contains(userAnswerTemp)){
					allPaths.add(userAnswerTemp);
					int endString =  userAnswerTemp.getAttributes().get("distinguishedName").toString().indexOf(",DC");
					int startString = userAnswerTemp.getAttributes().get("distinguishedName").toString().indexOf("OU=");
					String filter = userAnswerTemp.getAttributes().get("distinguishedName").toString().substring(startString,endString);
					recursiveLDAPExplorer(ctx, ctx.search(filter, null));
				}
			}
			else{
				allResults.add(userAnswerTemp);
			}
		}
	}*/
}
