package fr.imie.sel_Java.utils;

public class ConvertToBindingString {
	
	private static ConvertToBindingString IMPLEMENT = new ConvertToBindingString();

	public static ConvertToBindingString getConvertToBindingString(){
		return IMPLEMENT;
	}
	
	public static String convertToBindingString(byte[] objectGUID) {
		StringBuilder displayStr = new StringBuilder();

		displayStr.append(convertToDashedString(objectGUID));

		return displayStr.toString();
	}

	public static String convertToDashedString(byte[] objectGUID) {
		StringBuilder displayStr = new StringBuilder();

		displayStr.append(prefixZeros((int) objectGUID[3] & 0xFF));
		displayStr.append(prefixZeros((int) objectGUID[2] & 0xFF));
		displayStr.append(prefixZeros((int) objectGUID[1] & 0xFF));
		displayStr.append(prefixZeros((int) objectGUID[0] & 0xFF));
		displayStr.append(prefixZeros((int) objectGUID[5] & 0xFF));
		displayStr.append(prefixZeros((int) objectGUID[4] & 0xFF));
		displayStr.append(prefixZeros((int) objectGUID[7] & 0xFF));
		displayStr.append(prefixZeros((int) objectGUID[6] & 0xFF));
		displayStr.append(prefixZeros((int) objectGUID[8] & 0xFF));
		displayStr.append(prefixZeros((int) objectGUID[9] & 0xFF));
		displayStr.append(prefixZeros((int) objectGUID[10] & 0xFF));
		displayStr.append(prefixZeros((int) objectGUID[11] & 0xFF));
		displayStr.append(prefixZeros((int) objectGUID[12] & 0xFF));
		displayStr.append(prefixZeros((int) objectGUID[13] & 0xFF));
		displayStr.append(prefixZeros((int) objectGUID[14] & 0xFF));
		displayStr.append(prefixZeros((int) objectGUID[15] & 0xFF));

		return displayStr.toString();
	}

	public static String prefixZeros(int value) {
		if (value <= 0xF) {
			StringBuilder sb = new StringBuilder("0");
			sb.append(Integer.toHexString(value));

			return sb.toString();

		} else {
			return Integer.toHexString(value);
		}
	}
}
