package fr.imie.sel_Java.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
public class Topic extends EntityBase {

    public Topic() {
    }
    
    public Topic(Date deadline, Document document){
    	this.deadline = deadline;
    	this.documents.add(document);
    }
    
    @Column(unique=true)
    private String name;
    @Column
    private Date deadline;
    @OneToMany(cascade = {CascadeType.ALL})
	@JoinTable(
			name="topic_homework",
			joinColumns = @JoinColumn(name="topic_id"),
			inverseJoinColumns = @JoinColumn(name="homework_id"))
    private List<Homework> homeworks = new ArrayList<Homework>();
    
    @OneToMany(fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
	@JoinTable(
			name="topic_document",
			joinColumns = @JoinColumn(name="topic_id"),
			inverseJoinColumns = @JoinColumn(name="document_id"))
    private List<Document> documents = new ArrayList<Document>();
    @Transient
    private User author;
    
    @ManyToOne
    private Promotion promotion;
    
	public Date getDeadline() {
		return deadline;
	}

	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}

	public List<Homework> getHomeworks() {
		return homeworks;
	}

	public void setHomeworks(List<Homework> homeworks) {
		this.homeworks = homeworks;
	}

	public List<Document> getDocuments() {
		return documents;
	}

	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}

	public void removeDocumentByPath(String path){
		for (Document document : this.documents) {
			if(document.getPath().equals(path)){
				this.documents.remove(document);
				break;
			}
		}
	}
	
	public User getAuthor() {
		return author;
	}

	public void setAuthor(User author) {
		this.author = author;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Promotion getPromotion() {
		return promotion;
	}

	public void setPromotion(Promotion promotion) {
		this.promotion = promotion;
	}

}