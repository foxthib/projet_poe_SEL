package fr.imie.sel_Java.entity;

import java.util.ArrayList;

public class UserList {

	private ArrayList<User> users = new ArrayList<User>();
	
	public ArrayList<User> getUsers(){
		return users;
	}
	@SuppressWarnings("unchecked")
	public void setUsers(ArrayList<?> users) {
		this.users = (ArrayList<User>) users;
	}

}
