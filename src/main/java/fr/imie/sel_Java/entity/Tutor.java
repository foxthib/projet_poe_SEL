package fr.imie.sel_Java.entity;

import javax.persistence.Entity;
import javax.persistence.Inheritance;

@Entity
@Inheritance
public class Tutor extends User{
	
    public Tutor() {
    }
    
	public Tutor(String firstName,String lastName ){
    	this.setFirstName(firstName);
    	this.setLastName(lastName);
    }

    public Tutor(String firstName, String lastName, Promotion promotion, String objectGUID, String password,
			String email, String sAMAccountName){
		this.setFirstName(firstName);
		this.setLastName(lastName);
		this.setPromotion(promotion);
		this.setObjectGUID(objectGUID);
		this.setPassword(password);
		this.setEmail(email);
		this.setsAMAccountName(sAMAccountName);
    }
    
    public void Evaluate(Homework document) {
        
    }

}