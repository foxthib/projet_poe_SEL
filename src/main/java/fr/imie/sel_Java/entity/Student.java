package fr.imie.sel_Java.entity;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.ManyToOne;

@Entity
@Inheritance
public class Student extends User{
	
    public Student() {
    }

    public Student(String firstName,String lastName ){
    	this.setFirstName(firstName);
    	this.setLastName(lastName);
    }
    
    public Student(String firstName, String lastName, Promotion promotion, String objectGUID, String password,
			String email, String sAMAccountName){
		this.setFirstName(firstName);
		this.setLastName(lastName);
		this.setPromotion(promotion);
		this.setObjectGUID(objectGUID);
		this.setPassword(password);
		this.setEmail(email);
		this.setsAMAccountName(sAMAccountName);
    }

	@ManyToOne
    private Workgroup workgroup;
    
    public Workgroup getGroup() {
		return workgroup;
	}

	public void setGroup(Workgroup workgroup) {
		this.workgroup = workgroup;
	}

	public void joinGroup(String name) {
        
    }

    public Workgroup createGroup(String name) {
        
        return null;
    }

}