package fr.imie.sel_Java.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
public class Homework extends EntityBase {
	
	@Column
	private String title;
	@Column
    private Float mark;
	@ManyToOne
    private Topic topic;
	@Transient
	private User author;
	@OneToMany
    private List<Document> documents = new ArrayList<Document>();

	public Float getMark() {
		return mark;
	}

	public void setMark(Float mark) {
		this.mark = mark;
	}

	public Topic getTopic() {
		return topic;
	}

	public void setTopic(Topic topic) {
		this.topic = topic;
	}

	public List<Document> getDocuments() {
		return documents;
	}

	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}

	public void removeDocumentByPath(String path){
		for (Document document : this.documents) {
			if(document.getPath().equals(path)){
				this.documents.remove(document);
				break;
			}
		}
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}



}