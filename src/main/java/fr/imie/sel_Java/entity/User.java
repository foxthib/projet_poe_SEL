package fr.imie.sel_Java.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;


@Inheritance
@Entity
public class User extends EntityBase{
	
	@Column(name="firstname")
    private String firstName;
    @Column(name="lastname")
    private String lastName;
    @ManyToOne
    private Promotion promotion;
    @ManyToMany
    private List<Notification> notifications = new ArrayList<Notification>();
    @Column
    private String objectGUID;
    @Column
    private String password;
    @Column
    private String email;
    @Column(name="samaccountname")
    private String sAMAccountName;
	@ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
	@Fetch(FetchMode.SELECT)
    private List<Homework> homeworks  = new ArrayList<Homework>();
	
	@OneToMany(fetch = FetchType.EAGER,cascade = {CascadeType.ALL})
	@Fetch(FetchMode.SELECT)
	@JoinTable(
			name="user_document",
			joinColumns = @JoinColumn(name="user_id"),
			inverseJoinColumns = @JoinColumn(name="document_id"))
	private List<Document> documents  = new ArrayList<Document>();
	
	@OneToMany(fetch = FetchType.EAGER,cascade = {CascadeType.ALL})
	@Fetch(FetchMode.SELECT)
	@JoinTable(
			name="user_topic",
			joinColumns = @JoinColumn(name="user_id"),
			inverseJoinColumns = @JoinColumn(name="topic_id"))
	private List<Topic> topics = new ArrayList<Topic>();
	
	
	public User(){
		
	}

	public List<Homework> getHomeworks() {
		return homeworks;
	}

	public void setHomeworks(List<Homework> homeworks) {
		this.homeworks = homeworks;
	}
	
	public Homework findHomeworkByTitle(String title){
		for (Homework homework : this.homeworks) {
			if(homework.getTitle().equals(title)){
				return homework;
			}
		}
		return null;
	}
	
	public Homework findHomeworkById(Integer id){
		for (Homework homework : this.homeworks) {
			if(homework.getId().equals(id)){
				return homework;
			}
		}
		return null;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Promotion getPromotion() {
		return promotion;
	}

	public void setPromotion(Promotion promotion) {
		this.promotion = promotion;
	}

	public List<Notification> getNotifications() {
		return notifications;
	}

	public void setNotifications(List<Notification> notifications) {
		this.notifications = notifications;
	}

	public String getObjectGUID() {
		return objectGUID;
	}

	public void setObjectGUID(String objectGUID) {
		this.objectGUID = objectGUID;
	}

	public String getsAMAccountName() {
		return sAMAccountName;
	}

	public void setsAMAccountName(String sAMAccountName) {
		this.sAMAccountName = sAMAccountName;
	}

	public List<Document> getDocuments() {
		return documents;
	}

	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}

	public void removeDocumentByPath(String path){
		for (Document document : this.documents) {
			if(document.getPath().equals(path)){
				this.documents.remove(document);
				break;
			}
		}
	}
	
	public Topic findTopicByName(String name){
		for (Topic topic : this.topics) {
			if(topic.getName().equals(name)){
				return topic;
			}
		}
		return null;
	}
	
	public Topic findTopicById(Integer id){
		for (Topic topic : this.topics) {
			if(topic.getId().equals(id)){
				return topic;
			}
		}
		return null;
	}
	
	public List<Topic> getTopics() {
		return topics;
	}

	public void setTopics(List<Topic> topic) {
		this.topics = topic;
	}



}