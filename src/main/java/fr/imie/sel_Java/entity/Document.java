package fr.imie.sel_Java.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
public class Document extends EntityBase{

	public Document(){
		
	}
	
	public Document(String title, String path, User author){
		this.title = title;
		this.path = path;
		this.author = author;
	}
	
	@Column
    private String title;
	@Column(unique=true)
    private String path;
	@Transient
	private User author;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public User getAuthor() {
		return author;
	}

	public void setAuthor(User author) {
		this.author = author;
	}

}