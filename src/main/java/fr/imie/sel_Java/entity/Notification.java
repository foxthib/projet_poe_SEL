package fr.imie.sel_Java.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;

@Entity
public class Notification  extends EntityBase{
	
    public Notification() {
    }

    @Column
    private String message;
    @ManyToMany
    private List<User> recivers;

	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public List<User> getRecivers() {
		return (List<User>) recivers;
	}


	public void setRecivers(List<User> recivers) {
		this.recivers = recivers;
	}


	public void deleteNotification() {
        // TODO implement here
    }

}