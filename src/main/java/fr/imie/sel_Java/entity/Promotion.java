package fr.imie.sel_Java.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;

@Entity
public class Promotion  extends EntityBase{

	public Promotion() {
	}

	public Promotion(String name){
		this.name = name;
	}
	
	@Column(unique=true)
	private String name;
	@OneToMany(mappedBy="promotion")
	private List<User> members;
	
	@OneToMany(fetch  = FetchType.EAGER,cascade = {CascadeType.ALL})
	@JoinTable(
			name="promotion_topic",
			joinColumns = @JoinColumn(name="promotion_id"),
			inverseJoinColumns = @JoinColumn(name="topic_id"))
	private List<Topic> topics = new ArrayList<Topic>();
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<User> getMembers() {
		return members;
	}

	public void setMembers(List<User> members) {
		this.members = members;
	}

	public List<Topic> getTopics() {
		return topics;
	}

	public void setTopics(List<Topic> topics) {
		this.topics = topics;
	}

}