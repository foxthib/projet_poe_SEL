package fr.imie.sel_Java.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
public class Workgroup  extends EntityBase{
	
	@Column
    private String name;
	@OneToMany(mappedBy="workgroup")
    private List<Student> member = new ArrayList<Student>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Student> getMember() {
		return member;
	}

	public void setMember(List<Student> member) {
		this.member = member;
	}

	public void invitStudent(Student student) {
        // TODO implement here
    }

    public void removeStudent(String nameStudent) {
        // TODO implement here
    }

}