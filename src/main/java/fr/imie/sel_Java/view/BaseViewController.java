package fr.imie.sel_Java.view;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.imie.sel_Java.controller.BaseController;
import fr.imie.sel_Java.entity.EntityBase;

public class BaseViewController<T extends EntityBase> extends BaseController<T>{

	private static final String SELECT_ITEM = "SelectItem";
	private String baseName;
	
	private Class<T> classT;
	
	BaseViewController(Class<T> classT){
		this.classT = classT;
		baseName = classT.getSimpleName().toLowerCase();
	}
	
	@RequestMapping(path = SELECT_ITEM, method = RequestMethod.POST)
	public String selectItem(Model model, T selectItem){
		model.addAttribute("message","");
		model.addAttribute("selected"+baseName, selectItem);
		
		return SELECT_ITEM;
	}
}
