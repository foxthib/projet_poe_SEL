package fr.imie.sel_Java;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import javax.naming.directory.Attributes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import fr.imie.sel_Java.dao.*;
import fr.imie.sel_Java.dao.interfaces.*;
import fr.imie.sel_Java.entity.*;
import fr.imie.sel_Java.utils.*;

@SpringBootApplication
public class Main implements CommandLineRunner {

	private ArrayList<Tutor> tutors = new ArrayList<Tutor>();
	private ArrayList<Student> students = new ArrayList<Student>();
	private ArrayList<Promotion> promos = new ArrayList<Promotion>();

	@Autowired
	@Qualifier("StudentDAO")
	private StudentDAO studentDAO;
	@Autowired
	@Qualifier("TutorDAO")
	private TutorDAO tutorDAO;
	@Autowired
	@Qualifier("HomeworkDAO")
	private HomeworkDAO homeworkDAO;
	@Autowired
	@Qualifier("DocumentDAO")
	private DocumentDAO documentDAO;
	@Autowired
	@Qualifier("TopicDAO")
	private TopicDAO topicDAO;
	@Autowired
	@Qualifier("WorkgroupDAO")
	private WorkgroupDAO workgroupDAO;
	@Autowired
	@Qualifier("NotificationDAO")
	private NotificationDAO notificationDAO;
	@Autowired
	@Qualifier("PromotionDAO")
	private PromotionDAO promotionDAO;
	@Autowired
	@Qualifier("UserDAO")
	private UserDAO userDAO;

	public static void main(String[] args) {
		SpringApplication.run(Main.class, args);
		
	}
	
	@Override
	public void run(String... args){
		
		//if(Arrays.asList(args).contains("create")){
			Ldap ldap = new Ldap();
			getLdapUsers(ldap.ConnectToLdap());
			
			Tutor admin = new Tutor();
			admin.setObjectGUID("1");
			admin.setFirstName("admin");
			admin.setLastName("admin");
			admin.setsAMAccountName("admin");
			admin.setPassword("admin");
			tutors.add(0,admin);
				
			Promotion prom = new Promotion();
			prom.setName("PROMO_TEST");
			promos.add(0, prom);

			
				for (int i = 0; i < promos.size();i++){
					promos.set(i, promotionDAO.create(promos.get(i)));
				}
				
				Student stud = new Student();
				stud.setObjectGUID("2");
				stud.setFirstName("stud");
				stud.setLastName("stud");
				stud.setsAMAccountName("stud");
				stud.setPassword("stud");
				stud.setPromotion(promos.get(0));								
				students.add(0,stud);
				
				for (int i = 0; i < students.size();i++){
					students.set(i, (Student)userDAO.create(students.get(i)));
				}			

				for (int i = 0; i < tutors.size();i++){
					tutors.set(i, (Tutor)userDAO.create(tutors.get(i)));
				}	
		//}
	}
	
	public void getLdapUsers(List<Attributes> allResults) {
		
		SessionIdentifierGenerator sessionIdentifierGenerator =  new SessionIdentifierGenerator();
		
		for (Attributes res : allResults) {

			try {
				
				String firstName = "";
				if(res.get("givenName") != null) firstName = res.get("givenName").get().toString();
				String lastName = "";
				if(res.get("sn") != null) lastName = res.get("sn").get().toString();				
				String guid = ConvertToBindingString.convertToBindingString((byte[]) res.get("objectGUID").get());
				
				String sAMAccountName = "";
				if(res.get("sAMAccountName") != null) sAMAccountName = res.get("sAMAccountName").get().toString();
				else sAMAccountName = res.get("givenName").get().toString() + res.get("sn").get().toString();
				
				String password = "";
				if(res.get("userPassword") != null) password = res.get("userPassword").get().toString();
				else password = sessionIdentifierGenerator.nextSessionId();
		
				String email = "";
				if(res.get("email") != null) email = res.get("email").get().toString();
				
				Promotion myPromo = null;
				if (res.get("memberOf") != null){
					boolean contains = false;
					
					int indexStartString = res.get("memberOf").get().toString().indexOf("_");
					int indexStopString = res.get("memberOf").get().toString().indexOf(",OU");
					String subString = res.get("memberOf").get().toString().substring(indexStartString + 1, indexStopString);
					  
					for(Promotion promo:promos){
						if(promo.getName().equals(subString)){
							contains = true;
							myPromo = promo;
							break;
						}
					}
					
					if(!contains){
						myPromo = new Promotion(subString);
						promos.add(myPromo);
					}
				}
				
				if (myPromo != null && myPromo.getName().equals("Formateurs-Externes")) {
					Tutor toAdd = new Tutor(firstName, lastName, myPromo, guid, password, email, sAMAccountName);
					tutors.add(toAdd);
				}
				else{
					Student toAdd = new Student(firstName, lastName, myPromo, guid, password, email, sAMAccountName);
					students.add(toAdd);
				}
				
			} catch (Exception e) {
				System.out.println(e);
			}
		}
	}

	public final class SessionIdentifierGenerator {
		private SecureRandom random = new SecureRandom();

		public String nextSessionId() {
			return new BigInteger(130, random).toString(32);
		}
	}

	@Bean
	public ITutorDAO getTutorDao() {
		return new TutorDAO();
	}

	@Bean
	public IStudentDAO getStudentDao() {
		return new StudentDAO();
	}

	@Bean
	public IWorkgroupDAO getGroupDao() {
		return new WorkgroupDAO();
	}

	@Bean
	public IHomeworkDAO getHomeworkDao() {
		return new HomeworkDAO();
	}

	@Bean
	public INotificationDAO getNotificationDao() {
		return new NotificationDAO();
	}

	@Bean
	public IPromotionDAO getPromotionDao() {
		return new PromotionDAO();
	}

	@Bean
	public ITopicDAO getTopicDao() {
		return new TopicDAO();
	}

	@Bean
	public IDocumentDAO getDocumentDao() {
		return new DocumentDAO();
	}
	
	@Bean
	public IUserDAO getUserDao() {
		return new UserDAO();
	}
}