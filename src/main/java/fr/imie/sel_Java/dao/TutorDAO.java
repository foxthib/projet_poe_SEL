package fr.imie.sel_Java.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.imie.sel_Java.dao.interfaces.ITutorDAO;
import fr.imie.sel_Java.entity.Tutor;

@Transactional
@Component("TutorDAO")
public class TutorDAO  extends BaseDAO<Tutor> implements ITutorDAO{

	public TutorDAO() {
		super(Tutor.class);
	}
}
