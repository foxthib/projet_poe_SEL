package fr.imie.sel_Java.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.imie.sel_Java.dao.interfaces.INotificationDAO;
import fr.imie.sel_Java.entity.Notification;

@Transactional
@Component("NotificationDAO")
public class NotificationDAO extends BaseDAO<Notification> implements INotificationDAO  {
	
	public NotificationDAO(){
		super(Notification.class);
	}

}
