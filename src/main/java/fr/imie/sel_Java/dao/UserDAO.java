package fr.imie.sel_Java.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.imie.sel_Java.dao.interfaces.IUserDAO;
import fr.imie.sel_Java.entity.User;

@Transactional
@Component("UserDAO")
public class UserDAO extends BaseDAO<User> implements IUserDAO{

	public UserDAO(){
		super(User.class);
	}

	@Override
	public User create(User user){
		if(this.findByGUID(user.getObjectGUID()) == null){
			if(user.getPromotion() != null){
				user.setPromotion(promotionDAO.findByName(user.getPromotion().getName()));
			}
			return em.merge(user);
		}
		else{
			return em.merge(this.findByGUID(user.getObjectGUID()));
		}	
	}
	
	@Autowired @Qualifier("PromotionDAO")
    protected PromotionDAO promotionDAO;
	
 	public User findBysAMAccountName(String sAMAccountName) { 
 		Query query; 		
 		query = super.em.createQuery("select u from User u where u.sAMAccountName =:name"); 		
 		query.setParameter("name", sAMAccountName); 				
 		List<?> results = query.getResultList(); 	
 		
 		if(results.size()>0){ 			 
 			return (User) results.get(0); 		 
		} 		
 		
 		return null; 	
	}
 	
 	public User findByGUID(String objectGUID) { 
 		Query query; 		
 		query = super.em.createQuery("select u from User u where u.objectGUID =:objectGUID"); 		
 		query.setParameter("objectGUID", objectGUID); 				
 		List<?> results = query.getResultList(); 	
 		
 		if(results.size()>0){ 			 
 			return (User) results.get(0); 		 
		} 		
 		
 		return null; 	
	}
 	
}
