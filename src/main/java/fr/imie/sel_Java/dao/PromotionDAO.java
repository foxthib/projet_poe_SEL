package fr.imie.sel_Java.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.imie.sel_Java.dao.interfaces.IPromotionDAO;
import fr.imie.sel_Java.entity.Promotion;

@Transactional
@Component("PromotionDAO")
public class PromotionDAO extends BaseDAO<Promotion> implements IPromotionDAO {

	public PromotionDAO() {
		super(Promotion.class);
	}
	
	public Promotion findByName(String name){ 		
		Query query; 		
		query = super.em.createQuery("select p from Promotion p where p.name =:name"); 		
		query.setParameter("name", name); 				
		List<?> results = query.getResultList(); 		 
		if(results.size()>0){ 			 
			return (Promotion) results.get(0); 		 
		} 				
		return null; 	
	}
	
	@Override
	public Promotion create(Promotion promo){
		if(this.findByName(promo.getName()) == null){
			return em.merge(promo);
		}
		return this.findByName(promo.getName());
	}
}
