package fr.imie.sel_Java.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.imie.sel_Java.dao.interfaces.IWorkgroupDAO;
import fr.imie.sel_Java.entity.Workgroup;

@Transactional
@Component("WorkgroupDAO")
public class WorkgroupDAO extends BaseDAO<Workgroup> implements IWorkgroupDAO{

	public WorkgroupDAO(){
		super(Workgroup.class);
	}
	
	
}
