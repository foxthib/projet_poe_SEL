package fr.imie.sel_Java.dao.interfaces;

import org.springframework.stereotype.Repository;
import fr.imie.sel_Java.entity.Workgroup;

@Repository
public interface IWorkgroupDAO extends IBaseDAO<Workgroup> {

}
