package fr.imie.sel_Java.dao.interfaces;

import java.util.List;

public abstract interface IBaseDAO<T> {
	
	public T create(T item);
	
	public void delete(T item);
	
	@SuppressWarnings("rawtypes")
	public List getAll();
	
	public T getById(Integer id);
	
	public T update(T item);
}
