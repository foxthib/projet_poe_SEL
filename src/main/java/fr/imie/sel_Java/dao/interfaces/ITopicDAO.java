package fr.imie.sel_Java.dao.interfaces;

import org.springframework.stereotype.Repository;
import fr.imie.sel_Java.entity.Topic;

@Repository
public interface ITopicDAO extends IBaseDAO<Topic> {

}
