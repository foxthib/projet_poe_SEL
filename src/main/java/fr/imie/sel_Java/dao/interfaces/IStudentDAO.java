package fr.imie.sel_Java.dao.interfaces;

import org.springframework.stereotype.Repository;
import fr.imie.sel_Java.entity.Student;

@Repository
public interface IStudentDAO extends IBaseDAO<Student> {

}
