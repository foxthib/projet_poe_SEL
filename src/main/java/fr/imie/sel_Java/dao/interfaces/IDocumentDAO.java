package fr.imie.sel_Java.dao.interfaces;

import org.springframework.stereotype.Repository;
import fr.imie.sel_Java.entity.Document;

@Repository
public interface IDocumentDAO extends IBaseDAO<Document> {

}
