package fr.imie.sel_Java.dao.interfaces;

import org.springframework.stereotype.Repository;
import fr.imie.sel_Java.entity.Promotion;

@Repository
public interface IPromotionDAO extends IBaseDAO<Promotion> {

}
