package fr.imie.sel_Java.dao.interfaces;

import org.springframework.stereotype.Repository;
import fr.imie.sel_Java.entity.Notification;

@Repository
public interface INotificationDAO extends IBaseDAO<Notification> {

}
