package fr.imie.sel_Java.dao.interfaces;

import org.springframework.stereotype.Repository;
import fr.imie.sel_Java.entity.Tutor;

@Repository
public interface ITutorDAO extends IBaseDAO<Tutor> {

}
