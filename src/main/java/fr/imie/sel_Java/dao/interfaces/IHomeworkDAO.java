package fr.imie.sel_Java.dao.interfaces;

import org.springframework.stereotype.Repository;
import fr.imie.sel_Java.entity.Homework;

@Repository
public interface IHomeworkDAO extends IBaseDAO<Homework> {

}
