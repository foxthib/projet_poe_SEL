package fr.imie.sel_Java.dao.interfaces;

import org.springframework.stereotype.Repository;
import fr.imie.sel_Java.entity.User;

@Repository
public interface IUserDAO extends IBaseDAO<User> {

}
