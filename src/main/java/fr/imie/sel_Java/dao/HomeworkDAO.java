package fr.imie.sel_Java.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.imie.sel_Java.dao.interfaces.IHomeworkDAO;
import fr.imie.sel_Java.entity.Homework;

@Transactional
@Component("HomeworkDAO")
public class HomeworkDAO extends BaseDAO<Homework> implements IHomeworkDAO{

	public HomeworkDAO() {
		super(Homework.class);
	}
	
}
