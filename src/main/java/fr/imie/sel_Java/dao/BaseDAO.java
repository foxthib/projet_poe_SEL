package fr.imie.sel_Java.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import fr.imie.sel_Java.dao.interfaces.IBaseDAO;

@Transactional
public abstract class BaseDAO<T> implements IBaseDAO<T>{

	private Class<T> classT;
	
	public BaseDAO(){
		
	}
	
	public BaseDAO(Class<T> classT){
		this.classT = classT;
	}
	
	@PersistenceContext
	protected EntityManager em;
	
	@Override
	public T create(T t){
		em.persist(t);
		return null;
	}

	@Override
	public void delete(T t) {
		if (em.contains(t)){
			em.remove(t);
		}
		else{
			em.remove(em.merge(t));
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> getAll() {
		return em.createQuery("from " + classT.getSimpleName()).getResultList();
	}

	@Override
	public T getById(Integer id) {
		return em.find(classT, id);
	}

	@Override
	public T update(T t){
		return em.merge(t);
	}
	
}
