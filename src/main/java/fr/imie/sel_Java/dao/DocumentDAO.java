package fr.imie.sel_Java.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.imie.sel_Java.dao.interfaces.IDocumentDAO;
import fr.imie.sel_Java.entity.Document;

@Transactional
@Component("DocumentDAO")
public class DocumentDAO extends BaseDAO<Document> implements IDocumentDAO{

	public DocumentDAO() {
		super(Document.class);
	}
	
	public Document findByPath(String path){ 		
		Query query; 		
		query = super.em.createQuery("select d from Document d where d.path =:path"); 		
		query.setParameter("path", path); 				
		List<?> results = query.getResultList(); 		 
		if(results.size()>0){ 			 
			return (Document) results.get(0); 		 
			} 				
		return null; 	
	}
	
}
