package fr.imie.sel_Java.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.imie.sel_Java.dao.interfaces.ITopicDAO;
import fr.imie.sel_Java.entity.Topic;

@Transactional
@Component("TopicDAO")
public class TopicDAO extends BaseDAO<Topic> implements ITopicDAO{
	
	public TopicDAO(){
		super(Topic.class);
	}

}
