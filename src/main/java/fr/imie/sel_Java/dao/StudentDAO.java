package fr.imie.sel_Java.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.imie.sel_Java.dao.interfaces.IStudentDAO;
import fr.imie.sel_Java.entity.Student;


@Transactional
@Component("StudentDAO")
public class StudentDAO extends BaseDAO<Student> implements IStudentDAO {

	public StudentDAO() {
		super(Student.class);
	}
}
