<!DOCTYPE HTML>
<head>
<title>tutorHome</title>
<meta content="text/html ; charset=UTF-8" />
<link rel="icon" href="../../favicon.ico" />

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet" />

<!-- Custom styles for this template -->
<link href="starter-template.css" rel="stylesheet" />

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<link href="css/style.css" rel="stylesheet" />

<script src="jquery.js"></script>

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

</head>
<body>

	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Project SEL</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="tutor">Home</a></li>
					<li><a href="#about">About</a></li>
					<li><a href="#contact">Contact</a></li>
					<li class="right"> Connected as </br> ${ user.getFirstName() } ${ user.getLastName() } 
					</br> <a href="/login?logout=true">Log out</a></li>
				</ul>
			</div>
		</div>
	</nav>

<table width=100%>
<form method="post" enctype="multipart/form-data">

	<input type="submit" formaction="default_url_when_press_enter" style="visibility: hidden; display: none;">
	<tr>
	<td width=33%  valign="top">
	Topics : </br>
			<select name="selectedTopic" size="10" class="showBox">		
					<#if allTopics??>
						<#list allTopics as topic>
				  			<option value='${ topic.getId() }'> ${ topic.getId() } ${ topic.getName() }</option>			  			
						</#list>
					</#if>
			</select>
			</br></br>
			<button formaction="/tutorSelectTopic" class="btn btn-default">Select</button>
			<button formaction="/tutorDeleteTopic" class="btn btn-default">Delete</button>
			</br>
			</br><input type="text" name="nameTopic" placeholder="Name" style="font-style:italic">
			</br><input type="text" name="deadlineTopic" placeholder="Deadline mm/dd/yyyy" style="font-style:italic">
			</br>
			<select name="promotionTopic">
				<option selected disabled style="font-style:italic;color:grey">Select promotion</option> 
				<#if allPromotions??>
					<#list allPromotions as promotion>
			  			<option value='${ promotion.getId() }'> ${ promotion.getName() }</option>			  			
					</#list>
				</#if>
			</select>
			
			</br></br>
			<button formaction="/tutorCreateTopic" class="btn btn-default" >Create</button>
	</td>
	
	<td width=33% valign="top">
	Documents of <#if allTopics?? && selectedTopic??> <#list allTopics as topic> <#if topic.getId()==selectedTopic?number>${ topic.getName() }</#if> </#list></#if> :
	</br>
		
		<select multiple name="selectedFile" size="10" class="showBox">	
				<#if allDocs??>
					<#list allDocs as doc>
			  			<option value='${ doc.getPath() }'> ${ doc.getTitle() }</option>		  			
					</#list>
				</#if>
		</select>
		</br></br>
		<button formaction="/tutorDeleteDocuments" class="btn btn-default">Delete</button>
		<button formaction="/tutorDownloadDocuments" class="btn btn-default">Download</button>
		</br></br>
		<label class="btn btn-default btn-file"> Browse <input
			type="file" class="hidden" name=" uploadFiles[] " multiple/>
		</label> <input formaction="/tutorUpload" type="submit" class="btn btn-default" />
		
	</td>
	
	<td width=33% valign="top"> Homeworks :</br>
		<select multiple name="selectedHomework" size="10" class="showBox">	
				<#if allHomeworks??>
					<#list allHomeworks as homework>
			  			<option value='${ homework.getId() }' disabled> ${ homework.getTitle() }</option>	
			  			<#list homework.getDocuments() as document><option value='${ document.getPath() }' class="subSelect"> ${ document.getTitle() }</option></#list>	  			
					</#list>
				</#if>
		</select>
		</br></br>
		<button formaction="/tutorDownloadHomework" class="btn btn-default">Download</button>
				
	</td>
	
	</tr>
	</form>
</table>

	<#if message??>
		<#if message == "successUpload">
			<div class="alert alert-success" role="alert">
				<p>File(s) uploaded</p>
			</div>
		<#elseif message == "failedUpload">
			<div class="alert alert-danger" role="alert">
				<p>File(s) fail to upload : internal error</p>
			</div>
		<#elseif message == "duplicateUpload">
			<div class="alert alert-danger" role="alert">
				<p>File(s) fail to upload : duplicate(s)</p>
			</div>
		<#elseif message == "missingUploadDocuments">
			<div class="alert alert-success" role="alert">
				<p>File(s) fail to upload : missing argument(s)</p>
			</div>
		<#elseif message == "successDelete">
			<div class="alert alert-warning" role="alert">
				<p>File(s) deleted</p>
			</div>
		<#elseif message == "failedDelete">
			<div class="alert alert-danger" role="alert">
				<p>File(s) fail to delete : internal error</p>
			</div>
		<#elseif message == "unselectDownload">
			<div class="alert alert-warning" role="alert">
				<p>File(s) fail to download : no file selected</p>
			</div>	
		<#elseif message == "failedDownload">
			<div class="alert alert-danger" role="alert">
				<p>File(s) fail to download : internal error</p>
			</div>
		<#elseif message == "successDownload">
			<div class="alert alert-success" role="alert">
				<p>Download with success</p>
			</div>
		<#elseif message == "successDeleteTopic">
			<div class="alert alert-warning" role="alert">
				<p>Topic(s) delete</p>
			</div>
		<#elseif message == "failedDeleteTopic">
			<div class="alert alert-danger" role="alert">
				<p>Fail to delete topic(s)</p>
			</div>
		<#elseif message == "failedCreateTopic">
			<div class="alert alert-danger" role="alert">
				<p>Fail to create topic : internal error</p>
			</div>
		<#elseif message == "successCreateTopic">
			<div class="alert alert-success" role="alert">
				<p>Topic create with success</p>
			</div>
		<#elseif message == "missingCreateTopic">
			<div class="alert alert-warning" role="alert">
				<p>Fail to create topic : missing argument(s)</p>
			</div>	
		<#elseif message == "fromatFailedCreateTopic">
			<div class="alert alert-warning" role="alert">
				<p>Fail to create topic : error format argument(s)</p>
			</div>					
		</#if>
	</#if>

	<!-- Bootstrap core JavaScript
   ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<!-- <script>
	window.jQuery
			|| document
					.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')
</script>-->
	<script src="js/bootstrap.min.js"></script>
	<script src="js/autoClosedAlert.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="assets/js/ie10-viewport-bug-workaround.js"></script>

</body>
</html>