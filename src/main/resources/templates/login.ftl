<!DOCTYPE HTML>
<head>
<title>Login</title>
<link rel="icon" type="image/png" href="/favicon.png">
<meta content="text/html ; charset=UTF-8" />

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet" />

<!-- Custom styles for this template -->
<link href="starter-template.css" rel="stylesheet" />

<link href="css/style.css" rel="stylesheet" />

</head>
<body>

	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Project SEL</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="#">Home</a></li>
					<li><a href="#about">About</a></li>
					<li><a href="#contact">Contact</a></li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>

	<div class="container">
		<form method="POST" action="login">
			<input type="text" name="username" placeholder="username" style="font-style:italic"/> <br />
			<br /><input type="password" name="password" placeholder="password" style="font-style:italic"/> <br />
			<br /> <input type="submit" class="btn btn-default"> 
			
			<#if fail == "true" >
			<div class="alert alert-danger" role="alert">
				<p>Bad username or password !!!</p>
			</div>
			</#if>

			
		</form>

	</div>


	<!-- Bootstrap core JavaScript
   ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<!-- <script>
	window.jQuery
			|| document
					.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')
</script>-->
	<script src="js/bootstrap.min.js"></script>
	<script src="js/login.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="assets/js/ie10-viewport-bug-workaround.js"></script>

</body>
</html>